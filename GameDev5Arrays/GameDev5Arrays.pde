int ballCount = 10;
int x[];
int y[];
int speedY[];

void collide(){
  for(int i = 0; i < ballCount; i++){
    if(y[i]+10 >= height){
      speedY[i] = -speedY[i];
    }
  }
}

void setup(){
  x = new int[ballCount];
  y = new int[ballCount];
  speedY = new int[ballCount];
  for(int i = 0; i < ballCount; i++){
    x[i] = i*20;
    y[i] = 20;
    speedY[i] = 3;
  }
  y[4] = 30;
  size(600,400);
}
void draw(){
  background(0);
  for(int i = 0; i < ballCount; i++){
    ellipse(x[i],y[i],20,20);
    y[i] = y[i]+speedY[i];
  }
  collide();
}